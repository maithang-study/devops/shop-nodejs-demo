# pull the NodeJS Docker image
FROM node:15-alpine

# pull
LABEL MAINTAINER Mai Thang <maiducthang.it@gmail.com>

# create a new directory in the container
WORKDIR /app

# install pm2 
RUN npm install -g pm2

# copy the package.json and package-lock.json to the new directory
COPY package*.json ./

RUN chown -R node:node /app

USER node

# install libraries
RUN npm install

# copy the source files to the container
COPY . .

# export the port 5000
EXPOSE 3000

# start the app 
CMD ["pm2-runtime", "./config/pm2.json"]
